import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {RecipeService} from "../shared/recipe.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Recipe} from "../shared/recipe.model";

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.css']
})
export class AddRecipeComponent implements OnInit {

  recipeForm!: FormGroup;
  isEdit = false;
  editedId= '';

  constructor(private recipeService: RecipeService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.recipeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      image: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([])
    })

    this.route.data.subscribe (data => {
      const recipe = <Recipe | null> data.request;
      if (recipe) {
        this.isEdit = true;
        this.editedId = recipe.id;
        this.setFormValue({
          name: recipe.name,
          image: recipe.image,
          description: recipe.description,
          ingredients: recipe.ingredients,
          steps: recipe.steps
        })
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          name: '',
          image: '',
          description:'',
          ingredients:'',
          steps: '',
        })
      }
    })

  }


  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.recipeForm.setValue(value);
    });
  }

  saveRequest() {
    const recipe = new Recipe(
      this.recipeForm.value.id,
      this.recipeForm.value.name,
      this.recipeForm.value.image,
      this.recipeForm.value.description,
      this.recipeForm.value.ingredients,
      this.recipeForm.value.steps,
    );

    const next = () => {
      this.recipeService.fetchRecipes()
      void this.router.navigate(['']);
    };

    if (this.isEdit) {
      this.recipeService.editRequest(recipe).subscribe(next);
      alert('data updated!');
    } else {
      this.showForm();
    }
  }

  addSteps() {
    const steps = <FormArray>this.recipeForm.get('steps');
    const stepGroup = new FormGroup({
      stepImage: new FormControl('', Validators.required),
      stepDesk: new FormControl('', Validators.required),
    })
    steps.push(stepGroup);
  }

  getRecipes() {
    const steps =  <FormArray>(this.recipeForm.get('steps'));
    return steps.controls;
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.recipeForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }


  showForm() {
    const name = this.recipeForm.value.name;
    const description = this.recipeForm.value.description;
    const image = this.recipeForm.value.image;
    const ingredients = this.recipeForm.value.ingredients;
    const steps = this.recipeForm.value.steps;
    const body = {name, description,image, ingredients, steps};
    this.recipeService.sendRequest(body);
    this.redirect();
  }

  redirect() {
    this.recipeService.fetchRecipes();
    void this.router.navigate(['']);
  }
}
