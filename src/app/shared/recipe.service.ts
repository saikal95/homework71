import {Injectable} from "@angular/core";
import {Recipe} from "./recipe.model";
import {HttpClient} from "@angular/common/http";
import {Subject} from "rxjs";
import {map, tap} from "rxjs/operators";



@Injectable()
export class RecipeService {
  recipeChange = new Subject <Recipe[]>();
  recipeFetching = new Subject<boolean>();
  recipeRemoving = new Subject <boolean>();


  private recipes: Recipe[] = [];

  constructor(private http: HttpClient) {
  }

  sendRequest(body: Object){
    this.http.post('https://another-plovo-default-rtdb.firebaseio.com/recipes.json', body).subscribe();
  }

  fetchRecipes(){
    this.recipeFetching.next(true);
    this.http.get<{[id:string]:Recipe}>('https://another-plovo-default-rtdb.firebaseio.com/recipes.json')
      .pipe(map (result => {
        return Object.keys(result).map(id=> {
          const recipeData = result[id];
          return new Recipe(
            id,
            recipeData.name,
            recipeData.image,
            recipeData.description,
            recipeData.ingredients,
            recipeData.steps,
          );
        });
      }))
      .subscribe(recipes=>{
        this.recipes = recipes;
        this.recipeChange.next(this.recipes.slice());
        this.recipeFetching.next(false);
      }, () => {
        this.recipeFetching.next(false);
      })
  }

  removeRecipe(id: string) {
    this.recipeRemoving.next(true);

    return this.http.delete(`https://another-plovo-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(
      tap(() => {
        this.recipeRemoving.next(false);
      }, () => {
        this.recipeRemoving.next(false);
      })
    );
  }


  fetchRecipe(id: string) {
    return this.http.get<Recipe | null>(`https://another-plovo-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(
      map(result => {
        if(!result)return null;

        return new Recipe(
          id, result.name, result.image, result.description, result.ingredients,
          result.steps
        );
      })
    );
  }


  editRequest(recipe: Recipe){
    const body = {
      name: recipe.name,
      image: recipe.image,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    }

    return this.http.put(`https://another-plovo-default-rtdb.firebaseio.com/recipes/${recipe.id}.json`, body).pipe(
      tap());
  }


}
