export class Recipe{
  constructor(
    public id: string,
    public name: string,
    public image: string,
    public description: string,
    public ingredients: string,
    public steps: [{stepImage: string, stepDesk:string}],

  ) {}
}
