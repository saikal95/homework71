import {Component, OnDestroy, OnInit} from '@angular/core';
import {Recipe} from "../shared/recipe.model";
import {Subscription} from "rxjs";
import {RecipeService} from "../shared/recipe.service";

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit, OnDestroy {

   recipes!: Recipe[];
   recipesChangeSubscription!: Subscription;
   recipeFetchingSubscription!: Subscription;
   loading = false;

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {
    this.recipesChangeSubscription = this.recipeService.recipeChange.subscribe((recipes:Recipe[])=> {
      this.recipes = recipes;
    })

    this.recipeFetchingSubscription = this.recipeService.recipeFetching.subscribe((isFetching: boolean)=> {
      this.loading = isFetching;
    })

    this.recipeService.fetchRecipes();
  }


  ngOnDestroy() {
    this.recipesChangeSubscription.unsubscribe();
    this.recipeFetchingSubscription.unsubscribe();
  }

}
