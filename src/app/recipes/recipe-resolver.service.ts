import { Injectable } from '@angular/core';
import {RecipeService} from "../shared/recipe.service";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Recipe} from "../shared/recipe.model";
import {EMPTY, Observable, of} from "rxjs";
import {mergeMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class RecipeResolverService implements Resolve<Recipe> {

  constructor(private recipeService: RecipeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipe> | Observable<never>  {
    const recipeId = <string>route.params['id'];
    return this.recipeService.fetchRecipe(recipeId).pipe(mergeMap(recipe => {
      if (recipe) {
        return of(recipe);
      } else {
        void this.router.navigate(['']);
        return EMPTY;
      }
    }))

  }


}
