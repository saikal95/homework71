import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RecipesComponent} from "./recipes/recipes.component";
import {AddRecipeComponent} from "./add-recipe/add-recipe.component";
import {RecipeDetailsComponent} from "./recipe-details/recipe-details.component";
import {RecipeResolverService} from "./recipes/recipe-resolver.service";

const routes: Routes = [
  {path: '', component: RecipesComponent},
  {path: 'recipes/new', component: AddRecipeComponent},
  {path:':id/info/edit', component: AddRecipeComponent,
    resolve: {
      request: RecipeResolverService
    }
  },
  {path:':id/info', component: RecipeDetailsComponent,
    resolve: {
      request: RecipeResolverService
    }
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
