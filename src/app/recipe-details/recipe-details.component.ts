import { Component, OnInit } from '@angular/core';
import {Recipe} from "../shared/recipe.model";
import {ActivatedRoute} from "@angular/router";
import {RecipeService} from "../shared/recipe.service";

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {

  recipe! : Recipe;


  constructor(private route: ActivatedRoute, private recipeService: RecipeService ) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.recipe = <Recipe>data.request;

    })


  }

}
