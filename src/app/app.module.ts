import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddRecipeComponent } from './add-recipe/add-recipe.component';
import { RecipesComponent } from './recipes/recipes.component';
import { OneRecipeComponent } from './recipes/one-recipe/one-recipe.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {ReactiveFormsModule} from "@angular/forms";
import {RouterModule, ROUTES} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {RecipeService} from "./shared/recipe.service";
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';

@NgModule({
  declarations: [
    AppComponent,
    AddRecipeComponent,
    RecipesComponent,
    OneRecipeComponent,
    ToolbarComponent,
    RecipeDetailsComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule
    ],

  providers: [RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
